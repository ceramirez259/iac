#!/bin/bash

PROJECT_NAMESPACE=$1
PROJECT_NAME=$2
SOURCE_BRANCH=$3

echo "Making a clone from: https://$CI_SERVER_HOST/$PROJECT_NAMESPACE/$PROJECT_NAME"
git clone https://gitlab-ci-token:$CI_JOB_TOKEN@$CI_SERVER_HOST/$PROJECT_NAMESPACE/$PROJECT_NAME
cd $PROJECT_NAME
git checkout $SOURCE_BRANCH
echo '***********************'
echo 'PLEASE VERIFY THE REVISION WE WILL USE FOR THIS DEPLOYMENT.'
echo '***********************'
echo 'Revision:'
git rev-parse --short HEAD
git log -1 | cat
