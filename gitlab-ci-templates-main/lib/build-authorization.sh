#!/bin/bash

PRINCIPAL_GROUP_ID=$1

# Only Owners of the PRINCIPAL_GROUP_ID can publish a release candidate.
# https://docs.gitlab.com/ee/api/access_requests.html
RELEASE_ACCESS_LEVEL=50
# Only Owners, Maintainers or Developers of the PRINCIPAL_GROUP_ID can publish a snapshot package.
SNAPSHOT_ACCESS_LEVEL_D=30
SNAPSHOT_ACCESS_LEVEL_M=40
SNAPSHOT_ACCESS_LEVEL_O=50

# Identifying if the process will be a release or an snapshot.
#    If the action is performed by a Tag, it is a release, otherwise a snapshot.
if [ -z "$CI_COMMIT_TAG" ]; then
  BUILD_TYPE="SNAPSHOT"
else
  BUILD_TYPE="RELEASE"
fi

echo '***********************'
echo "starting build authorization for a ${BUILD_TYPE}"
echo '***********************'

if [[ "$BUILD_TYPE" == "RELEASE" ]]; then
  RESP=$(curl -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$CI_API_V4_URL/groups/$PRINCIPAL_GROUP_ID/members" | jq ".[] | select(.access_level | contains($RELEASE_ACCESS_LEVEL)) | select(.id | contains($GITLAB_USER_ID))")
  if [[ "$RESP" != "" ]]; then
    echo "Authorization Granted to: $GITLAB_USER_EMAIL"
    exit 0
  else
    echo '@@@@@@@@@@@@@@@@@@@@@@@ AUTHORIZATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
    echo 'You are not allowed to make Release Candidates in this project, contact your Product Owner.'
    exit 1
  fi
fi

if [[ "$BUILD_TYPE" == "SNAPSHOT" ]]; then
  RESP_D=$(curl -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$CI_API_V4_URL/groups/$PRINCIPAL_GROUP_ID/members" | jq ".[] | select(.access_level | contains($SNAPSHOT_ACCESS_LEVEL_D)) | select(.id | contains($GITLAB_USER_ID))")
  RESP_M=$(curl -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$CI_API_V4_URL/groups/$PRINCIPAL_GROUP_ID/members" | jq ".[] | select(.access_level | contains($SNAPSHOT_ACCESS_LEVEL_M)) | select(.id | contains($GITLAB_USER_ID))")
  RESP_O=$(curl -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$CI_API_V4_URL/groups/$PRINCIPAL_GROUP_ID/members" | jq ".[] | select(.access_level | contains($SNAPSHOT_ACCESS_LEVEL_O)) | select(.id | contains($GITLAB_USER_ID))")
  if [[ "$RESP_D" != "" ]] || [[ "$RESP_M" != "" ]] || [[ "$RESP_O" != "" ]]; then
    echo "Authorization Granted to: $GITLAB_USER_EMAIL"
    exit 0
  else
    echo '@@@@@@@@@@@@@@@@@@@@@@@ AUTHORIZATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
    echo 'You are not allowed to make Snapshots in this project, contact your Product Owner.'
    exit 1
  fi
fi

echo '@@@@@@@@@@@@@@@@@@@@@@@ BUILD_TYPE ERROR @@@@@@@@@@@@@@@@@@@@@@@'
echo 'Only SNAPSHOT or RELEASE allowed'
exit 1