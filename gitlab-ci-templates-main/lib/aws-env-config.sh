#!/bin/bash

echo '***********************'
echo "starting aws-env-config for env:${ENV}"
echo '***********************'
KEY_VAR_NAME=${ENV}_AWS_ACCESS_KEY_ID
eval "export AWS_ACCESS_KEY_ID=\$$KEY_VAR_NAME"
SECRET_VAR_NAME=${ENV}_AWS_SECRET_ACCESS_KEY
eval "export AWS_SECRET_ACCESS_KEY=\$$SECRET_VAR_NAME"
REGION_VAR_NAME=${ENV}_AWS_REGION
eval "export AWS_REGION=\$$REGION_VAR_NAME"

if [[ "$AWS_ACCESS_KEY_ID" == "" ]]; then
    echo "Missing GitLabCi Variable AWS_ACCESS_KEY_ID"
    exit 1
fi

if [[ "$AWS_SECRET_ACCESS_KEY" == "" ]]; then
    echo "Missing GitLabCi Variable AWS_SECRET_ACCESS_KEY"
    exit 1
fi

if [[ "$AWS_REGION" == "" ]]; then
    echo "Missing GitLabCi Variable AWS_REGION"
    exit 1
fi

mkdir ~/.aws
bash -c  "cat > ~/.aws/config" << EOF
[default]
region = $AWS_REGION
EOF
