#!/bin/bash

echo '***********************'
echo 'starting deployment configuration validation'
echo '***********************'

if [[ -f "common.yaml" ]] || [[ -f "common.yml" ]]; then
  echo '@@@@@@@@@@@@@@@@@@@@@@@ COMMON.YAML VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo "Our GitLab Pipeline was upgraded, please follow instructions in https://fintools.atlassian.net/wiki/spaces/FCS/pages/36143113/CON-208+-+GitLab+Pipeline+improvements+authorization+shared+environments"
  exit 1
fi

# ENV validation.
if [[ "$ENV" == "TEST" ]] || [[ "$ENV" == "PROD" ]] || [[ "$ENV" == "SHARED" ]]; then
  echo "ENV validation: passed"
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo 'ENV should be TEST, PROD or SHARED'
  exit 1
fi
# CLUSTER_PROVIDER validation.
if [[ "$CLUSTER_PROVIDER" == "EKS" ]] || [[ "$CLUSTER_PROVIDER" == "HETZNER" ]]; then
  echo "CLUSTER_PROVIDER validation: passed"
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo 'CLUSTER_PROVIDER should be EKS or HETZNER'
  exit 1
fi
# REPO_CHANNEL validation.
#   - Case: (1) Gitlab Registry
if [[ "$CHART_PROJECT_ID" != "" ]]; then
  if [[ "$REPO_CHANNEL" == "helm-snapshots" ]] || [[ "$REPO_CHANNEL" == "helm-releases" ]]; then
    echo "REPO_CHANNEL validation: passed"
  else
    echo '@@@@@@@@@@@@@@@@@@@@@@@ VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
    echo 'REPO_CHANNEL should be helm-snapshots or helm-releases'
    exit 1
  fi
fi
# Branch validation.
#   if $ENV is test, the branch name should begin with test_
ENV_BRANCH=$(cut -d"_" -f1 <<<$CI_COMMIT_REF_NAME)
ENV_BRANCH=$(echo $ENV_BRANCH | tr a-z A-Z)
if [[ "$ENV_BRANCH" == "TEST" ]] || [[ "$ENV_BRANCH" == "PROD" ]] || [[ "$ENV_BRANCH" == "SHARED" ]]; then
  echo "branch name validation: passed"
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ BRANCH VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo 'if $ENV is test, the branch name should begin with test_'
  exit 1
fi
# ENV_YML_FILE validation. 
#   if the branch name is test_aws, ENV_YML_FILE should be test_aws.yaml
export ENV_YML_FILE="$CI_COMMIT_REF_NAME.yaml"
if [[ -f "$ENV_YML_FILE" ]]; then
  echo "ENV_YML_FILE validation: passed"
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ ENV_YML_FILE VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo "$ENV_YML_FILE does not exists and it is mandatory."
  exit 1
fi
# Helm Repository validation.
#   - Case: (1) Gitlab Registry
if [[ "$CHART_PROJECT_ID" != "" ]]; then
  echo 'Adding Helm Repository from GitLab.'
  helm repo add --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD finoa-chart-registry "${CI_API_V4_URL}/projects/$CHART_PROJECT_ID/packages/helm/$REPO_CHANNEL"
  export HELM_REPO="${CI_API_V4_URL}/projects/$CHART_PROJECT_ID/packages/helm/$REPO_CHANNEL"
elif [[ "$CHART_REPOSITORY_URL" != "" ]]; then
  # - Case: Public Helm Repository
  echo "Adding Helm Repository from Public Repo: $CHART_REPOSITORY_URL"
  helm repo add finoa-chart-registry "$CHART_REPOSITORY_URL"
  export HELM_REPO="$CHART_REPOSITORY_URL"
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ CHART REPOSITORY VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo "You should define either CHART_PROJECT_ID or CHART_REPOSITORY_URL."
  exit 1
fi
echo 'Repo Update'
helm repo update
# Chart Name and Version existence validation.
if [[ "$CHART_VERSION" != "" ]] && [[ "$CHART_NAME" != "" ]]; then
  echo 'CHART_VERSION and CHART_NAME defined in the pipeline: passed'
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ CHART NAME/VERSION VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo "You should define both CHART_NAME and CHART_VERSION."
  exit 1
fi
# Chart Version in Helm Repository existence validation.
if [[ $CHART_VERSION == "latest" ]]; then
  export CHART_VERSION=$(helm search repo $CHART_NAME -o json | jq -r ".[0].version")
  echo "Latest version of chart has found: $CHART_VERSION"
  # else: CHART_VERSION will be specified number in the pipeline
fi
CHART_VERSION_IN_REPO=$(helm search repo $CHART_NAME --version $CHART_VERSION -o json | jq -r ".[0].version")
if [[ $CHART_VERSION_IN_REPO == $CHART_VERSION ]]; then
  echo "CHART_VERSION found in the helm repository: passed"
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ CHART VERSION VALIDATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo "CHART_VERSION=$CHART_VERSION was not found in the repository."
  exit 1
fi