#!/bin/bash

CLUSTER_VAR_NAME=${ENV}_${CLUSTER_PROVIDER}_CLUSTER

echo "Configuring kubectl for $CLUSTER_PROVIDER..."

if [[ $CLUSTER_PROVIDER == "EKS" ]]; then
  source $BASH_LIB_PATH/aws-env-config.sh
  eval "CLUSTER=\$$CLUSTER_VAR_NAME"
  aws eks --region $AWS_REGION update-kubeconfig --name $CLUSTER
elif [[ $CLUSTER_PROVIDER == "HETZNER" ]]; then
  bash -c "$BASH_LIB_PATH/get-file-from-gitlab-var.sh ~/.kube $CLUSTER_VAR_NAME config"
  chmod 600 ~/.kube/config
  export KUBECONFIG=~/.kube/config
else
  echo '@@@@@@@@@@@@@@@@@@@@@@@ CLUSTER_PROVIDER ERROR @@@@@@@@@@@@@@@@@@@@@@@'
  echo 'CLUSTER_PROVIDER should be EKS or HETZNER'
  exit 1
fi