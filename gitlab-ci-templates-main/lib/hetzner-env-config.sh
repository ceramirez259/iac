#!/bin/bash

echo '***********************'
echo "starting hetzner-env-config for env:${ENV}"
echo '***********************'
HCLOUD_VARNAME=${ENV}_HCLOUD_TOKEN
eval "export HCLOUD_TOKEN=\$$HCLOUD_VARNAME"
if [[ "$HCLOUD_TOKEN" == "" ]]; then
    echo "Missing GitLabCi Variable HCLOUD_TOKEN"
    exit 1
fi
