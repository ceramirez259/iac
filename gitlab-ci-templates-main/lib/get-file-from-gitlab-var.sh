#!/bin/bash

FOLDER=$1
GITLABCI_VARNAME=$2
FILENAME=$3

echo "Downloading file from Gitlab Variable $GITLABCI_VARNAME in ${FOLDER}/${FILENAME}"

if [[ "$FOLDER" == "" ]]; then
  echo "Missing FOLDER variable"
  exit 1
fi
if [[ "$GITLABCI_VARNAME" == "" ]]; then
  echo "Missing GITLABCI_VARNAME Value."
  exit 1
fi
if [[ "$FILENAME" == "" ]]; then
  echo "Missing Filename Value."
  exit 1
fi

mkdir -p $FOLDER
eval "cat \$$GITLABCI_VARNAME > ${FOLDER}/${FILENAME}"
echo 'Listing files in folder ${FOLDER}'
ls ${FOLDER}
