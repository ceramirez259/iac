#!/bin/bash

PRINCIPAL_GROUP_ID=$1

# Only Owners of the PRINCIPAL_GROUP_ID can deploy to prod or shared.
# https://docs.gitlab.com/ee/api/access_requests.html
PROD_ACCESS_LEVEL_TO_DEPLOY=50
# Only Owners or Maintainers of the PRINCIPAL_GROUP_ID can deploy to test.
TEST_ACCESS_LEVEL_TO_DEPLOY_M=40
TEST_ACCESS_LEVEL_TO_DEPLOY_O=50

echo '***********************'
echo "starting deployment authorization for env:${ENV}"
echo '***********************'

if [[ "$ENV" == "PROD" ]] || [[ "$ENV" == "prod" ]] || [[ "$ENV" == "SHARED" ]] || [[ "$ENV" == "shared" ]]; then
  RESP=$(curl -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$CI_API_V4_URL/groups/$PRINCIPAL_GROUP_ID/members" | jq ".[] | select(.access_level | contains($PROD_ACCESS_LEVEL_TO_DEPLOY)) | select(.id | contains($GITLAB_USER_ID))")
  if [[ "$RESP" != "" ]]; then
    echo "Authorization Granted to: $GITLAB_USER_EMAIL"
    exit 0
  else
    echo '@@@@@@@@@@@@@@@@@@@@@@@ AUTHORIZATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
    echo 'You are not allowed to trigger deployments to prod or shared environment.'
    exit 1
  fi
fi

if [[ "$ENV" == "TEST" ]] || [[ "$ENV" == "test" ]]; then
  RESP_M=$(curl -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$CI_API_V4_URL/groups/$PRINCIPAL_GROUP_ID/members" | jq ".[] | select(.access_level | contains($TEST_ACCESS_LEVEL_TO_DEPLOY_M)) | select(.id | contains($GITLAB_USER_ID))")
  RESP_O=$(curl -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$CI_API_V4_URL/groups/$PRINCIPAL_GROUP_ID/members" | jq ".[] | select(.access_level | contains($TEST_ACCESS_LEVEL_TO_DEPLOY_O)) | select(.id | contains($GITLAB_USER_ID))")
  if [[ "$RESP_M" != "" ]] || [[ "$RESP_O" != "" ]]; then
    echo "Authorization Granted to: $GITLAB_USER_EMAIL"
    exit 0
  else
    echo '@@@@@@@@@@@@@@@@@@@@@@@ AUTHORIZATION ERROR @@@@@@@@@@@@@@@@@@@@@@@'
    echo 'You are not allowed to trigger deployments to test environment.'
    exit 1
  fi
fi

echo '@@@@@@@@@@@@@@@@@@@@@@@ ENVIRONMENT ERROR @@@@@@@@@@@@@@@@@@@@@@@'
echo 'Pipeline require $ENV with one of the following options: TEST, PROD, SHARED'
exit 1